function GREEN_TO_DO_LIST()
{
	var self = this;
	self.list = [];

	self.selectedElement = null;
	self.UI = {
		'list' : $("#to-do-list")
	};

	self.changes = 0;
}

GREEN_TO_DO_LIST.prototype.update = function()
{
	var self = this;
	self.changes ++;
	self.updateHours();
};

GREEN_TO_DO_LIST.prototype.updateHours = function ()
{
	var self = this;
	var hours_total = 0;
	for (var i = 0; i < self.list.length; i ++)
	{
		if(parseInt(self.list[i].data.hours))
			hours_total += parseInt(self.list[i].data.hours);
	}
	$("#hours-label").html("Hours: " + hours_total);
};

GREEN_TO_DO_LIST.prototype.autosave = function ()
{
    // Autosave every 10s if there have been over 30 keystrokes
    var self = this;
    self.updateHours();
    
    if (self.changes >= 1) {
    		self.save();
    }
    setTimeout(function () {
    	self.autosave();
    }, 3000);
};

/*Adding new list element*/
GREEN_TO_DO_LIST.prototype.add_element = function(data, should_save, should_select)
{
	var self = this;

	var number = self.list.length + 1;

	var list_item = new LIST_ELEMENT();
	list_item.parent = self;
	list_item.initialize(number, data);

	self.list.push(list_item);
	self.UI.list.append(list_item.element);

	list_item.addEvents();

	//select
	if(should_select == null)
		should_select = true;

	if(should_select)
		self.select_element(list_item.element);

	//save
	if(should_save == null)
		should_save = true;

	if(should_save)
		self.save();
};

/*Selecting a list element*/
GREEN_TO_DO_LIST.prototype.select_element = function(element)
{
	var self = this;
	for(var i = 0; i < self.list.length; i ++)
	{
		if(self.list[i].element.hasClass("selected"))
			self.list[i].element.removeClass("selected");
	}

	if(element)
	{
		self.selectedElement = element;
		self.selectedElement.addClass("selected");
	}
};

GREEN_TO_DO_LIST.prototype.select_next = function(element)
{
	var self = this;

	var index = self.list.indexOf(element);
	var next = self.list[index + 1];

	if(next != null)
	{
		if(next.data.archive == 1)
		{
			self.select_next(next);
		}
		else 
		{
			self.select_element(next.element);
			next.inputs.description.focus();

			var len = next.inputs.description.val().length * 2;
			next.inputs.description[0].setSelectionRange(len, len);
		}
	}
	else 
	{
		self.add_element();
	}
};


/*Saving list*/
GREEN_TO_DO_LIST.prototype.save = function()
{
	var self = this;
	var data = $("#list-form").serializeArray();
	$("#loader").css("opacity", 1);

	$.ajax({
		url: save_list_url,
		type: 'POST',
		data: data,
		dataType: 'JSON',
		error: function (e)
		{
			alert("Error Saving: Form expired");

			location.reload();
		},
		success: function (data) {
			console.log(data);
		},
		complete: function (e)
		{
			$("#loader").css("opacity", 0);
			self.changes = 0;
		}
	});
};