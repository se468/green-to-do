$(document).ready(function(){
    var list_created = false;
    var list = new GREEN_TO_DO_LIST();

    
    function initList()
    {
        $("#to-do-list").css("display","block");
        $("#btn-area").css("display","block");
        $("#info-text").css("display", "none");
        list_created = true;

        

        list.autosave();
    }

        if(list_element.length > 0)//loading existing one
        {
            for(var i = 0; i < list_element.length; i ++)
            {
                list.add_element(list_element[i], false, false);
            }
            initList();
        }

        function handle_start_input()
        {
            if(!list_created)
            {
                initList();
                list.add_element(list_element[i], false, false);
            }
        }

        function addList ()
        {
            if(list_created)
            {
                list.add_element();
            }
        }

        $(window).keypress(function(e) {
            var key = e.which;
            handle_start_input();

        });

        $(window).click(function(e){
            handle_start_input();
            if(e.target == $("body")[0])
            {
                if(list.selectedElement)
                {
                    list.selectElement(null);
                }
            }
        });

        Mousetrap.bind(['command+n', 'ctrl+n'], function(e) {
            addList ();
            return false;
        });
        $("#add-list-btn").click(function(){
            addList ();
        });


        $("#to-do-list").sortable({
            items: "li:not(.ui-state-disabled)",
            update: function( event, ui ) {
                list.save();
            }
        });

        $("#archive-checkbox").click(function(){
            if($(this).is(':checked'))
            {
                $("#to-do-list").addClass("show-archived");
            }
            else 
            {
                $("#to-do-list").removeClass("show-archived");
            }
        });

        var clipboard = new Clipboard('#share-btn');

        clipboard.on('success', function(e) {
            alert("Copied URL to clipboard!");
        });

        $('#add-to-favorites-btn').click(function() {
            if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
              window.sidebar.addPanel(document.title, window.location.href, '');
            } else if (window.external && ('AddFavorite' in window.external)) { // IE Favorite
              window.external.AddFavorite(location.href, document.title);
            } else if (window.opera && window.print) { // Opera Hotlist
              this.title = document.title;
              return true;
            } else { // webkit - safari/chrome
              alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
          }
      });
    });