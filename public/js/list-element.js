function LIST_ELEMENT ()
{
	var self = this;
	self.parent = null;
	self.element = null;
	self.data = null;

	self.inputs = {
		description: null,
		note: null
	};
};

LIST_ELEMENT.prototype.initialize = function (number,data)
{
	var self = this;
	if(data == null)
	{
		data = {
			number : number,
			description: "",
			note: "",
			priority: "low", 
			status: "waiting",
			archive: 0
		};
	}
	else 
	{
		data.number = number;
	}

	self.data = data;


	var template = $("#list-element-template").html();
	var html = Mustache.render(template, data);

	self.element= $(html);


	self.element.attr('class','list-group-item ' + "status-" + self.data.status);

	if(self.data.archive == 1)
	{
		self.element.addClass("archived");
	}


};

LIST_ELEMENT.prototype.addEvents = function()
{
	var self = this;

	self.element.click(function(e){
		self.parent.select_element(self.element);
	});

	self.element.contextmenu(function(e) {
		//e.preventDefault();
		self.parent.select_element(null);
	});

	var section = self.element.find("#list-section-" + self.data.number);
	section.editable({
		title: 'Section Name',
		success: function(response, newValue) {
			var id = $(this).attr("id").replace("list-section-", "");
			$("#list-section-input-" + id).val(newValue);
			self.parent.save();
		}
	});

	var hours = self.element.find("#list-hours-" + self.data.number);
	hours.editable({
		title: 'Hours',
		success: function(response, newValue) {
			var id = $(this).attr("id").replace("list-hours-", "");
			$("#list-hours-input-" + id).val(newValue);
			self.parent.save();
		}
	});

	//priority
	var priority = self.element.find("#list-priority-" + self.data.number);
	priority.editable({
		source: [
		{value: "low", text: 'low'},
		{value: "medium", text: 'medium'},
		{value: "high", text: 'high'}
		],
		success: function(response, newValue) {
			var id = $(this).attr("id").replace("list-priority-", "");
			$("#list-priority-input-" + id).val(newValue);
			self.parent.save();
		}
	});

    	//status
    	var status = self.element.find("#list-status-" + self.data.number);
    	status.editable({
    		source: [
    		{value: "waiting", text: 'waiting'},
    		{value: "developing", text: 'developing'},
    		{value: "done", text: 'done'},
    		{value: "edit", text: 'edit'},
    		{value: "approved", text: 'approved'},
    		{value: "issue", text: 'issue'}
    		],
    		success: function(response, newValue) {
    			var id = $(this).attr("id").replace("list-status-", "");
    			$("#list-status-input-" + id).val(newValue);
    			self.parent.save();
    			self.element.attr('class','list-group-item ' + "status-" + newValue);
	     	//self.element.addClass("status-" + newValue);
	     }
	});

    	self.inputs.description = self.element.find("#list-description-" + self.data.number);
    	self.inputs.description.on('keydown', function(e) { 
    		var keyCode = e.keyCode || e.which;
    		if (keyCode == 9) { 
    			e.preventDefault(); 
    			self.parent.select_next(self);
    		}

    		self.parent.update();
    	});
    	self.inputs.description.focus();


    	self.inputs.note = self.element.find("#list-note-" + self.data.number);
    	self.inputs.note.on('keydown', function(e) { 
    		var keyCode = e.keyCode || e.which;
    		self.parent.update();
    	});

    	//Archive
    	var archive_btn = self.element.find("#list-archive-" + self.data.number); 
    	archive_btn.click( self.element , function(e){
    		e.preventDefault();
    		if(self.data.archive == 0)
    		{
    			self.element.addClass("archived");
    			$("#list-archive-input-"+ self.data.number).val(1);
    		}
    		else 
    		{
    			self.element.removeClass("archived");
    			$("#list-archive-input-"+ self.data.number).val(0);
    		}

    		
    		self.parent.save();
    	});


    	//Remove
    	var remove_btn = self.element.find("#list-remove-" + self.data.number);
    	remove_btn.click( self.element , function(e){
    		e.preventDefault();

    		if (confirm("Are you sure you want to remove?") == true) {
    			self.element.remove();
    			self.parent.save();
    		} else {
    			return false;
    		}
    		
    	});
    };

//Add Media List element should be able to add media