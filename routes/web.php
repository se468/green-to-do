<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/welcome',[
    		'pageTitle' => "Green-to-do",
    	]);
});

Route::get('generate', 'WorkspaceController@create')->name('generate');

Route::get('workspace/{code}', 'WorkspaceController@index')->name('workspace');
Route::post('workspace', 'WorkspaceController@update');
Route::post('workspace-rename', 'WorkspaceController@rename');