@include('includes.header')
@yield('extra-includes')
</head>
<body>
	<div class="main-container white-bg">
		@include('includes.navigation')

		@yield('main-content')


		<footer>
			<div class="push-1">
				<div class="footer__copyright-text">
					2017 Designed / Developed by Seyong Cho
				</div>
			</div>
		</footer>
	</div>
	
</body>
</html>
