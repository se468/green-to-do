@include('includes.header')
@yield('extra-includes')
</head>
<body>
	<div class="main-container white-bg">
		@yield('main-content')
	</div>
</body>
</html>
