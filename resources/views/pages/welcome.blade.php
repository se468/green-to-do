@extends('master')

@section('extra-includes')

@stop

@section('main-content')


<div class="flex-center position-ref full-height">
    <div class="top-right links">
        <a href="" data-toggle="modal" data-target="#about-modal">About</a>
    </div>
    <a href="{{ url('generate')}}" style="text-decoration: none; color: #636b6f;">
        <div class="title m-b-md text-center">
            Click to create a new list
        </div>
    </a>
</div>


<div class="modal fade" id="about-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">About</h4>
            </div>

            <div class="modal-body">


                Green to-do is a minimal and simple to-do list generator. It is <a href="https://bitbucket.org/se468/green-to-do" target="blank">open-source</a> project, developed & maintained by <a href="http://seyongcho.com" target="blank">Seyong Cho</a>.
                <br><br>

                Simply focus on writing / maintaining your to-do list, and worry less about creating accounts and learning to do all complex functionalities. 

                <br><br>


                It aims to replace using spreadsheets for your everyday to-do list.

                <br><br> 

                Do you share your to-do lists with clients or consultants using google drive? Well then, this tool will give you exactly the same functionality, but makes it simpler by giving it a consistent language and UI. 

                <br><br>
                
                Built by a freelancer, the app tries to understand the pain and work involved in organizing your day-to-day work. 

                <br><br>
                
                This app focuses on one functionality: making a to-do list. It doesn't do anything else, because I want the app to do the one thing better than everything else. 

                <br><br>
                
                When you make a list, you will generate an encrypted url that you can send to yourself and people involved in your work. 
                


                <br><br>
                Best of all, it is <b>completely free to use, forever</b>!

                <br><br>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@stop