<li class="list-group-item">
	<div class="row">
		<div class="col-xs-2 text-center">
			<div class="row">
				<div class="col-xs-3">
					{{ number }}
				</div>
				<div class="col-xs-5">

					<a href="#" id="list-section-{{ number }}" data-type="text">{{ section }}</a>

					<input type="hidden" class="form-control" id="list-section-input-{{ number }}" name="list[{{ number }}][section]" value="{{ section }}">
				</div>
				<div class="col-xs-4">
					<a href="#" id="list-hours-{{ number }}" data-type="text">{{ hours }}</a>

					<input type="hidden" class="form-control" id="list-hours-input-{{ number }}" name="list[{{ number }}][hours]" value="{{ hours }}">
				</div>
			</div>
			
		</div>

		<div class="col-xs-4">
			<textarea type="text" class="form-control"
			name="list[{{ number }}][description]" 
			id="list-description-{{ number }}">{{ description }}</textarea>
		</div>


		<div class="col-xs-2 text-center">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<a href="#" id="list-priority-{{ number }}" data-type="select" data-title="Select priority"> {{ priority }} </a>
						
						<input type="hidden" name="list[{{ number }}][priority]" id="list-priority-input-{{ number }}" value="{{ priority }}">

					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<a href="#" id="list-status-{{ number }}" data-type="select" data-title="Select status"> {{ status }} </a>
						
						<input type="hidden" name="list[{{ number }}][status]" id="list-status-input-{{ number }}" value="{{ status }}">

					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-1 text-center">
			<button class="btn btn-success" type="button" data-toggle="modal" data-target="#image-upload-modal">
				<i class="glyphicon glyphicon-plus"></i>
			</button>
		</div>

		<div class="col-xs-2">
			<!-- NOTE-->
			<textarea class="form-control"
			name="list[{{ number }}][note]" 
			id="list-note-{{ number }}">{{ note }}</textarea>
		</div>

		<div class="col-xs-1">
			<!-- ARCHIVE-->
			<input type="hidden" name="list[{{ number }}][archive]" id="list-archive-input-{{ number }}" value="{{ archive }}">
			<button class="btn list-archive-btn btn-empty" id="list-archive-{{ number }}" type="button"  data-toggle="tooltip" title="Archive">
				<i class="glyphicon glyphicon-folder-close"></i>
			</button>

			<!-- REMOVE-->
			<button class="btn list-remove-btn btn-empty" id="list-remove-{{ number }}" type="button"  data-toggle="tooltip" title="Remove">
				<i class="glyphicon glyphicon-trash"></i>
			</button>
		</div>
	</div>
</li>