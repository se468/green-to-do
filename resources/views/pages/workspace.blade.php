@extends('master')

@section('extra-includes')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<link rel="stylesheet" href="{{ URL::asset('css/pages/workspace.css') }}">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<script src="{{ URL::asset('js/vendor/mustache/mustache.js') }}"></script>
<script src="{{ URL::asset('js/vendor/mousetrap.min.js') }}"></script>
<script src="{{ URL::asset('js/vendor/clipboard.min.js') }}"></script>

<script src="{{ URL::asset('js/to-do-list.js') }}"></script>
<script src="{{ URL::asset('js/list-element.js') }}"></script>


<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var workspace = {!! $workspace !!};
    var list_element = {!! $workspace->list_element !!};
    var save_list_url = "{{ url('workspace') }}";
</script>
<script src="{{ URL::asset('js/workspace.js') }}"></script>

@stop

@section('main-content')
<div id="info-text" class="flex-center position-ref full-height white-bg">
    <div class="title m-b-md text-center">
        Start typing
    </div>
</div>

<form id="list-form" method="post" action="{{ url('workspace') }}">
    {{ csrf_field() }}
    <input type="hidden" name="workspace_id" value="{{ $workspace->id }}">

    <ul id="to-do-list" class="list-group to-do-list" style="display: none; margin-bottom: 0px;">
        <li class="list-group-item to-do-list--header  ui-state-disabled">
            <div class="row">
                <div class="col-xs-2 text-center">
                    <div class="row">
                        <div class="col-xs-3">
                            <button class="btn btn-success" type="button"  data-toggle="modal" data-target="#settings-modal"><i class="glyphicon glyphicon-cog"></i></button>
                        </div>
                        <div class="col-xs-5">
                            Section
                        </div>
                        <div class="col-xs-4" id="hours-label">
                            Hours
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    Description
                </div>
                <div class="col-xs-2">
                    <div class="row">
                        <div class="col-xs-6">
                            Priority
                        </div>
                        <div class="col-xs-6">
                            Status
                        </div>
                    </div>
                </div>
                <div class="col-xs-1">
                    Image
                </div>
                <div class="col-xs-2">
                    Note
                </div>
                <div class="col-xs-1">
                    <input type="checkbox" id="archive-checkbox"> <br/ >
                    Show Archived
                </div>
            </div>
        </li>
    </ul>
    <div class="row" id="btn-area" style="display: none;">
        <div class="col-md-12">
            <button class="btn btn-block btn-success btn-lg" type="button" id="add-list-btn">
                Add New
            </button>
        </div>
    </div>
</form>



<div class="bg-instructions text-center">
    <div class="bg-instructions--header">
        INSTRUCTIONS
    </div>
    Use <b>tab</b> to focus next list item or create another item <br>

    You can <b>share</b> the url with your team<br>
    <b>Drag</b> to sort items

    <div class="loader" style="opacity: 0;"></div>
</div>


<!-- Mustache Templates -->
<script id="list-element-template" type="text/template">
    @include('pages/workspace/list-element')
</script>


<!-- Modal -->
<div class="modal fade" id="image-upload-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Image or Video for reference</h4>
            </div>

            <div class="modal-body">
                <div class="text-center">Upload</div>
                <div class="text-center">or</div>
                <div class="text-center">Paste link</div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="settings-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">
                    List Settings 

                    <button class="btn btn-success pull-right" id="add-to-favorites-btn"><i class="glyphicon glyphicon-star"></i> &nbsp;Add list to Favorites</button>
                    <!--
                    <button class="btn btn-success pull-right" id="share-btn" data-clipboard-text="{{ request()->url() }}"><i class="glyphicon glyphicon-heart"></i> &nbsp;Share URL</button>
                    -->
                </h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <form action="{{ url('workspace-rename') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="workspace_id" value="{{ $workspace->id }}">
                            <div class="form-group">
                                <label>Name the list:</label>
                                <input type="text" class="form-control" name="name">
                            </div>

                            <input type="submit" value="Submit" class="btn btn-success btn-block">
                        </form>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@stop