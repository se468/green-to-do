## Green to-do
Green to-do is a minimal and simple to-do list generator.


Simply focus on writing / maintaining your to-do list, and worry less about creating accounts and learning to do all complex functionalities. 



It aims to replace using spreadsheets for your everyday to-do list.


Do you share your to-do lists with clients or consultants using google drive? Well then, this tool will give you exactly the same functionality, 

Built by a freelancer, the app tries to understand the pain and work involved in organizing your day-to-day work. 


This app focuses on one functionality: making a to-do list. It doesn't do anything else, because I want the app to do the one thing better than everything else. 


When you make a list, you will generate an encrypted url that you can send to yourself and people involved in your work. 


Best of all, it is completely free to use, forever!

## License

Green to-do is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
