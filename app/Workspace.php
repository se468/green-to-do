<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
	protected $table = 'workspaces';

    	public function list_element ()
    	{
    		return $this->hasMany("App\ListElement", "workspace_id");
    	}
}
