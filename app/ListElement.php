<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListElement extends Model
{
	protected $table = 'list_element';

	protected $fillable = [
			'workspace_id','section', 'hours', 'description', 'priority', 'note', 'status', 'archive'
	];

	//hasMany Images
}
