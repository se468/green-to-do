<?php
namespace App\Http\Controllers;

use App\ListElement;
use App\Workspace;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class WorkspaceController extends Controller
{
	public function create ()
	{
		$new_workspace = Workspace::create();
		$new_workspace->code = Crypt::encryptString($new_workspace->id);
		$new_workspace->save();

		return redirect()->route('workspace', ['code' => $new_workspace->code]);
	}

	public function index($code)
	{
		$workspace = Workspace::where(["code"=>$code])->first();
		
		return view('pages/workspace', [
			'pageTitle' => $workspace->title,
			'workspace' => $workspace
			]);
	}

	public function update ()
	{
		$input = request()->all();
		Log::info($input);

		DB::transaction(function () use ($input){
			$workspace = Workspace::find($input["workspace_id"]);
			$list_elements = $workspace->list_element;

			
			foreach ($list_elements as $each_element)
			{
				$each_element->delete();
			}

			foreach ($input['list'] as $each_list_data)
			{
				$each_element = ListElement::create($each_list_data);
				$each_element->workspace_id = $workspace->id;
				$each_element->save();
			}
		});
		$response = array(
			'status' => 'success',
			'msg' => 'Save Success',
			);

		return response()->json($response);
	}

	public function rename ()
	{
		$input = request()->all();
		$workspace = Workspace::find($input["workspace_id"]);
		$workspace->title = $input["name"];
		$workspace->save();

		return redirect()->back();
	}
}